const mongoose = require("mongoose");
const Posts = mongoose.model('posts');
let path = require("path");
var absPath = path.join(__dirname, "../view");

exports.baseroutes = async (req, res) => {
    res.sendFile(absPath + "/index.html");
};

exports.getPosts = async (req, res) => {
    const post = await Posts.find();
    res.json(post);
};
exports.getSinglePost = async (req, res) => {
    let postId = req.params.id
    const post = await Posts.findById({ _id: postId }, (err, data) => {
        if (err) {
            res.status(500).json({ messgae: "Some Wen Wrong" });
        } else {
            res.status(200).json({ messgae: "data found", data });
        }
    });

};
exports.updatePost = async (req, res) => {
    let postId = req.params.id
    const post = await Posts.findByIdAndUpdate({ _id: postId },{$set:req.body}, (err, data) => {
        if (err) {
            res.status(500).json({ messgae: "Some Wen Wrong" });
        } else {
            res.status(200).json({ messgae: "data Post Update", data });
        }
    });

};
exports.deletePost = async (req, res) => {
    let postId = req.params.id
    const post = await Posts.deleteOne({ _id: postId }, (err, data) => {
        if (err) {
            res.status(500).json({ messgae: "Some Wen Wrong" });
        } else {
            res.status(200).json({ messgae: "data Post Update", data });
        }
    });

};