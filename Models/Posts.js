const mongoose =require("mongoose");
const postSchema=new mongoose.Schema(
    {
        title:{
            type : String
        },
        author:{
            type : String
        },
        desc:{
            type : String
        }
    }
);
let Posts=mongoose.model('posts',postSchema);
module.exports=Posts;