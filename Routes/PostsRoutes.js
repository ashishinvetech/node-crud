// const mongoose =require("mongoose");
// const Posts =mongoose.model('posts');
const express = require("express");
const router=express.Router();



const postController=require("../Controllers/PostController");
router.get("/",postController.baseroutes);
router.get("/getPosts",postController.getPosts);
router.get("/getPosts/:id",postController.getSinglePost);
router.put("/getPosts/:id/update",postController.updatePost);
router.delete("/getPosts/:id",postController.getSinglePost);
module.exports=router;