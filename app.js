const express = require("express");

const app = express();
const routes=require("./Routes/PostsRoutes");
app.use("/",routes);
module.exports=app;