const mongoose =require("mongoose");
mongoose.Promise=global.Promise;
require('dotenv').config({path:'.env'});
mongoose.connect(process.env.DATABASE,{
    useUnifiedTopology:true,
    useNewUrlParser:true
});

mongoose.connection.on("error",(err)=>{
    console.errror(`Database connection error->${err.message}`);
});

require("./Models/Posts");
const app =require("./app");
const port=3000;
app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
  })
